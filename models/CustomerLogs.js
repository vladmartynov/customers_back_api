const mongoose = require('mongoose');

const { Schema } = mongoose;

const customerLogSchema = new Schema({
  customerId:{ 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Customer'
  },
  type:String,
  text:String,
  date: {
    type: Date,
    default: Date.now
  },
});

const CustomerLogs = mongoose.model('customerLog', customerLogSchema);

module.exports = CustomerLogs;
