const mongoose = require('mongoose');

const { Schema } = mongoose;

const customerSchema = new Schema({
  customerId:Number,
  locationId:{ 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Location'
  },
  firstName:String,
  lastName:String,
  email:String,
  phone:String,
  createdDate: {
    type: Date,
    default: Date.now
  },
});

const Customers = mongoose.model('customer', customerSchema);

module.exports = Customers;
