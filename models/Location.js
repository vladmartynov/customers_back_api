const mongoose = require('mongoose');

const { Schema } = mongoose;

const locationSchema = new Schema({
  locationId: mongoose.Schema.Types.ObjectId,
  name:String,
  createdDate: {
    type: Date,
    default: Date.now
  },
});

const Locations = mongoose.model('location', locationSchema);

module.exports = Locations;
