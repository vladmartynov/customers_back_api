const express = require('express');
const path = require('path');
const cors = require('cors');
const helmet = require('helmet');
const logger = require('./logger/loggerMiddleware');

const indexRouter = require('./routes/index');

require('./db');

const app = express();

app.use(helmet());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/api', indexRouter);

app.use(logger);

module.exports = app;
