const mongoose = require('mongoose');

// connect to db
mongoose.connect(
  `mongodb://localhost:27017/testdb`,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
  },
  (err) => {
    if (err) throw err;

    console.log('Successfully connected');
  },
);

module.exports.mongoose = mongoose;
