const Users = require('../../models/Users');

module.exports = {
  async getLoggedUserData(req, res, next) {
    try {
      const [data] = await Users.find({ _id: req.user._id });
      const {
        email,
        firstName,
        lastName,
        gender,
        dateOfBirth,
        _id: id,
        file,
        aboutMe,
        phoneNumber,
        preferences,
      } = data;
      const dataToSend = {
        userInfo: {
          gender,
          firstName,
          lastName,
          email,
          phoneNumber,
          dateOfBirth,
          aboutMe,
          id,
          file,
          preferences,
        },
      };
      res.status(200).send(dataToSend);
    } catch (err) {
      next(err);
    }
  },
  async changeUserPreferences(req, res, next) {
    try {
      const {
        user: { _id },
        body,
      } = req;
      await Users.findByIdAndUpdate(_id, { preferences: body });
      res.status(200).send(body);
    } catch (e) {
      next(e);
    }
  },
  async changeUserInformation(req, res, next) {
    try {
      const {
        user: { _id },
        body,
      } = req;
      await Users.findByIdAndUpdate(_id, { ...body });
      const result = await Users.findById(_id);
      const {
        email,
        firstName,
        lastName,
        gender,
        dateOfBirth,
        _id: id,
        file,
        aboutMe,
        phoneNumber,
        preferences,
      } = result;
      const dataToSend = {
        gender,
        firstName,
        lastName,
        email,
        phoneNumber,
        dateOfBirth,
        aboutMe,
        id,
        file,
        preferences,
      };
      res.status(200).send(dataToSend);
    } catch (e) {
      next(e);
    }
  },
};
