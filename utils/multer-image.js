const multerImages = require('multer');
const path = require('path');

const fileFilter = (req, file, cb) => {
  const isMimeTypeSupported = /jpeg|jpg|png|gif/.test(file.mimetype);
  const isExtensionValid = /jpeg|jpg|png|gif/.test(
    path.extname(file.originalname).toLowerCase(),
  );
  if (isMimeTypeSupported && isExtensionValid) return cb(null, true);
  const error = new Error('File type not supported.');
  error.status = 400;
  return cb(error);
};

const storage = multerImages.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/uploads/');
  },
  filename: (req, file, cb) => {
    cb(
      null,
      `${file.fieldname}-${Date.now()}-${file.originalname}`,
    );
  },
});

const upload = multerImages({
  storage,
  fileFilter,
});

module.exports = upload;
