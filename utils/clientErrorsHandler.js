const logger = require('../logger');

module.exports = (req, res, next) => {
  try {
    logger.fe(req.body.stack);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
};
